(require 'use-package)
(use-package elementaryx-early-init)
;; (use-package elementaryx-minimal) ;; already required by elementaryx base
;; (use-package elementaryx-base) ;; already required by elementaryx-org, elementaryx-dev-minimal, elementaryx-write
(use-package elementaryx-org)
(use-package elementaryx-ox-publish)
;; (use-package elementaryx-dev-minimal) ;; already required by elementaryx-dev and elementaryx-dev-parentheses
(use-package elementaryx-dev)
(use-package elementaryx-dev-parentheses)
(use-package elementaryx-write)
(use-package elementaryx-treemacs)
(use-package elementaryx-all-the-icons)

(provide 'elementaryx-full)
